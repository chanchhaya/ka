package org.khmeracademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import javax.swing.*;

@SpringBootApplication
public class WebUIApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder springApplicationBuilder){
        return springApplicationBuilder.sources(WebUIApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebUIApplication.class,args);
    }

}
