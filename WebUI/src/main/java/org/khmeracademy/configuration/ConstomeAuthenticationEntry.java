package org.khmeracademy.configuration;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class ConstomeAuthenticationEntry implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        System.out.println("Attamping..."+httpServletRequest.getRequestURI());
//        httpServletRequest.getSession().setAttribute("REDIRECT_SUCCESS_URL",httpServletRequest.getRequestURI()+ "?" + httpServletRequest.getQueryString());

        String redirectUrl = httpServletRequest.getRequestURI();

        String playlistid = httpServletRequest.getParameter("playlistid");
        String youtubeid = httpServletRequest.getParameter("youtubeid");
        if (playlistid != null && youtubeid != null) {
            redirectUrl += "?playlistid=" + playlistid + "&youtubeid=" + youtubeid;
        }

        httpServletRequest.getSession().setAttribute("REDIRECT_SUCCESS_URL", redirectUrl);
        httpServletResponse.sendRedirect("/login");
    }
}
