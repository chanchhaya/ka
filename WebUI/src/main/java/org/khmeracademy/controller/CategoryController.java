package org.khmeracademy.controller;

import org.khmeracademy.models.util.Encryption;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CategoryController {
    private CategoryServices categoryServices;
    @Autowired
    private  HomeController homeController=new HomeController(categoryServices);
    static int index=0;

    public CategoryController(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @GetMapping("/elearning/{id}")
    public String categorylevle1(ModelMap m, @PathVariable("id") int id) {
        List<CategoryForm> maincategories=categoryServices.getcategorieslevel1(0);
        m.addAttribute("categorieslevel1",maincategories);
        List<CategoryForm> categories=categoryServices.getcategorieslevel1(id);
        m.addAttribute("categorieslevel2",categories);
        return "category/cate-lvl-4";
    }


    @GetMapping("/elearning/course/{id}")
    public String categoryLevel2And3(ModelMap m, @PathVariable("id") String id) {
        List<CategoryForm> maincategories=categoryServices.getcategorieslevel1(0);
        m.addAttribute("categorieslevel1",maincategories);
        int decodeId=Integer.valueOf(Encryption.decode(id));

        List<CategoryForm> categorieslevel2=categoryServices.getcategorieslevel1(decodeId);
        m.addAttribute("categorieslevel2",categorieslevel2);

        List<List<CategoryForm>> categories=new ArrayList<>();
        if(!categorieslevel2.isEmpty())
            for (int i=0;i< categorieslevel2.size();i++){
                categories.add(categoryServices.getcategorieslevel1(categorieslevel2.get(i).getId()));
            }
        if(!categories.isEmpty()) {
            m.addAttribute("categorieslevel3", categories);
        }
        return "category/cate-lvl-4";
    }
}
