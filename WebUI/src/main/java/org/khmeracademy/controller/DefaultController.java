package org.khmeracademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultController {
    @GetMapping("/default")
    public String defaultLayout() {
        return "layouts/default";
    }
}
