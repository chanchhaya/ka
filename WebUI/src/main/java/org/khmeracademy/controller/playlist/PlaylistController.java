package org.khmeracademy.controller.playlist;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.playlist.Playlists;
import org.khmeracademy.services.CategoryServices;
import org.khmeracademy.services.playlist.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
public class PlaylistController {

    private CategoryServices categoryServices;

    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    private PlaylistService playlistService;

    @Autowired
    public PlaylistController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

//    @GetMapping("/search")
//    public String searchByPlaylistName(@RequestParam(value = "keyword", required = false) String name, ModelMap model) {
//        System.out.println("Playlist: " + name);
//        List<Categories> categories = categoryServices.getcategorieslevel1(0);
//        model.addAttribute("categorieslevel1", categories);
//        List<Playlists> playlists = playlistService.searchByPlaylistName(name);
//        model.addAttribute("playlistname",name);
//        model.addAttribute("playlists", playlists);
//        if (playlists.isEmpty()) {
//            System.out.println("Empty");
//            model.addAttribute("status", false);
//        } else {
//            System.out.println("A lot");
//            model.addAttribute("status", true);
//        }
//        return "category/search";
//    }

    @GetMapping("/search")
    public String search(@RequestParam(value = "keyword", required = false) String name, ModelMap model) {
        List<CategoryForm> categories = categoryServices.getcategorieslevel1(0);
        model.addAttribute("categorieslevel1", categories);
        model.addAttribute("playlistname", name);
        return "category/search";
    }


    @GetMapping("/search/fragment")
    public String SearchByPlaylistNameFragment(@RequestParam(value = "keyword", required = false) String name, ModelMap model) {
        List<Playlists> playlists = playlistService.searchByPlaylistName(name);
        model.addAttribute("playlistname", name);
        model.addAttribute("playlists", playlists);
        if (playlists.isEmpty()) {
            model.addAttribute("status", false);
        } else {
            model.addAttribute("status", true);
        }
        return "ajax/search-fragment :: searchList";
    }
}
