package org.khmeracademy.controller;


import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.user.User;
import org.khmeracademy.services.CategoryServices;
import org.khmeracademy.services.UserServiceImpl;
import org.khmeracademy.services.user.DepartmentService;
import org.khmeracademy.services.user.UniversiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Controller
public class UserController {
    private CategoryServices categoryServices;
    private DepartmentService departmentService;
    private UniversiryService universiryService;
    private UserDetails users;
    private UserServiceImpl userService;
    private static List<CategoryForm> categories=new ArrayList<>();


    @Autowired
    private void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }

    @Value("${spring.upload.server.path}")
    String serverPath;

    @Autowired
    private void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Autowired
    private void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Autowired
    private void setUniversiryService(UniversiryService universiryService) {
        this.universiryService = universiryService;
    }

    @GetMapping("/register")
    public String register2() {
        return "user/register-info";
    }

    @GetMapping("/profile")
    public String profile(ModelMap map) {
        categories=categoryServices.getcategorieslevel1(0);
        map.addAttribute("categorieslevel1",categories);
        map.addAttribute("users",new User());
        map.addAttribute("department",departmentService.findAllDepartment());
        map.addAttribute("university",universiryService.findAllUniversity());

        return "user/user-profile";
    }

    @PostMapping("/update")
    public String update(@Valid @ModelAttribute User user, @RequestParam("file") MultipartFile multipartFile, BindingResult result, ModelMap model) throws IOException {
        if (result.hasErrors()) {
            System.out.println("Error True");
            model.addAttribute("users", user);
            model.addAttribute("department", departmentService.findAllDepartment());
            model.addAttribute("university", universiryService.findAllUniversity());
            return "user/user-profile";
        }

        String fileName = UUID.randomUUID() + "." + multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf(".") + 1);
        if (!multipartFile.isEmpty()) {
            //Check Folder
            File path = new File(this.serverPath);
            if (!path.exists()) path.mkdirs();
            //Copy Image to Folder
            Files.copy(multipartFile.getInputStream(), Paths.get(serverPath, fileName));
            user.setUserimageurl(fileName);
            user.setUserimageurl("/image/"+user.getUserimageurl());
        }else{
            user.setUserimageurl(userService.findOneUserByFacebookID(user.getSc_fb_id()).getUserimageurl().toString());
        }
        System.out.println("User:" + user);
        Boolean check = userService.userUpdate(user);
        if (check == true) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        return "redirect:/profile";
    }

    @GetMapping("/history/delete/{id}")
    public String historyDelete(@PathVariable("id") int id) {
        System.out.println("ID" + id);
        System.out.println("Delete:" + userService.historyDelete(id));
        userService.historyDelete(id);
        return "redirect:/profile";
    }


}
