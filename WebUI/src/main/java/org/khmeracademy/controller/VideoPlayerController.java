package org.khmeracademy.controller;

import org.khmeracademy.models.ListVideo;
import org.khmeracademy.models.util.Encryption;
import org.khmeracademy.services.CategoryServices;
import org.khmeracademy.services.ListVideoService;
import org.khmeracademy.services.UserServiceImpl;
import org.khmeracademy.models.comment.Comment;
import org.khmeracademy.repositoy.comment.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class VideoPlayerController {
    private List<ListVideo> listVideos=new ArrayList<>();
    private ListVideoService listVideoService;
    private UserServiceImpl userService;
    private CommentService commentService;
    private CategoryServices categoryServices;

    @Autowired
    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }
    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
    public VideoPlayerController(ListVideoService listVideoService) {
        this.listVideoService = listVideoService;
    }
    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }


    @GetMapping("/elearning/video/watch")
    public String playVideo2(@RequestParam("playlistid") String playlistid, @RequestParam("youtubeid") String youtubeid,ModelMap modelMap){
        Integer decodeId=Integer.valueOf(Encryption.decode(playlistid));
        listVideos=listVideoService.listVideo(decodeId);
        modelMap.addAttribute("categorieslevel1", categoryServices.getcategorieslevel1(0));
        modelMap.addAttribute("listvideoplay",listVideos);
        modelMap.addAttribute("playlistid",playlistid);
        modelMap.addAttribute("comments",commentService.getComment(20));
        return "videoPlayer";
    }


    @GetMapping("/elearning/watch")
    public String mainWatch(){
        return "ajax/player-fragment :: main-player";
    }


    @GetMapping("/bookmark/Insert/{facebookId}/{urlVideo}")
    public String bookmarkInsert(@PathVariable("facebookId") String facebookId,@PathVariable("urlVideo")String urlVideo){
        System.out.println("FacebookId"+facebookId);
        System.out.println("FacebookId"+urlVideo);
        return "videoPlayer";
    }
}
