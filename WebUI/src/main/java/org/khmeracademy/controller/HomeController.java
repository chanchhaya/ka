package org.khmeracademy.controller;

import org.khmeracademy.models.Statistics;
import org.khmeracademy.models.category.Category;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.services.CategoryServices;
import org.khmeracademy.services.StatisticServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jws.WebParam;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    private CategoryServices categoryServices;
    @Autowired
    private StatisticServices statisticServices;

    public static List<CategoryForm> categories=new ArrayList<>();
    public static List<Statistics> statistics=new ArrayList<>();


    public HomeController(CategoryServices categoryServices) {
        this.categoryServices  = categoryServices;
    }

    @GetMapping("/")
    public String homeController(ModelMap modelMap) {
        categories=categoryServices.getcategorieslevel1(0);
        statistics=statisticServices.statistics();
        modelMap.addAttribute("categorieslevel1",categories);
        modelMap.addAttribute("statistic",statistics);
        return "home/index";
    }


    @GetMapping("/section-one")
    public String sectionOne(){
        return "ajax/section-one :: section-one";
    }


    @GetMapping("/section-two")
    public String sectionTwo(Model model){
        model.addAttribute("sectionTwo", categoryServices.findSectionTwo());
        return "ajax/section-two :: section-two";
    }

    @GetMapping("/section-three")
    public String sectionTree(Model model){
        model.addAttribute("sectionThree",categoryServices.findSectionThree());
        return "ajax/section-three :: section-three";
    }



}
