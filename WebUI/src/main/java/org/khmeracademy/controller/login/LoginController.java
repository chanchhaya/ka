package org.khmeracademy.controller.login;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.json.JSONObject;
import org.khmeracademy.models.user.AccountKitPostRequest;
import org.khmeracademy.models.user.Role;
import org.khmeracademy.models.user.User;
import org.khmeracademy.services.UserServiceImpl;
import org.khmeracademy.services.user.DepartmentService;
import org.khmeracademy.services.user.UniversiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Controller
//@RequestMapping("/facebook")
@PropertySource("classpath:fblogin.properties")
public class LoginController {

    // variable for facebook login

    @Value("${fb.login.appid}")
    private String FB_APP_ID;

    @Value("${fb.login.secret}")
    private String FB_APP_SECRET;

    @Value("${fb.login.domain}")
    private String DOMAIN;

    // virable for sms login
    @Value("${fb.app.id}")
    private String FB_AK_APP_ID;

    @Value("${fb.accountkit.secret}")
    private String AK_APP_SECRET;

    private static final String ME_ENDPOINT_BASE_URL = "https://graph.accountkit.com/v1.1/me";
    private static final String TOKEN_EXCHANGE_BASE_URL = "https://graph.accountkit.com/v1.1/access_token";

    private RestTemplate restTemplate = new RestTemplate();
    private User user = new User();

    // for facebook login
    // user detail service
    private UserServiceImpl userService;

    @Autowired
    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    // department service
    private DepartmentService departmentService;

    @Autowired
    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    // university service
    private UniversiryService universiryService;

    @Autowired
    public void setUniversiryService(UniversiryService universiryService) {
        this.universiryService = universiryService;
    }

    private static final String CALLBACK_URL = "/facebook/callback";

    private static final List<String> SCOPES = new ArrayList<String>() {
        private static final long serialVersionUID = 1L;

        {
            add("public_profile");
            add("email");
            add("user_gender");
        }
    };

    // Facebook API get user information
    private static final String USER_PROFILE_API_URL = "https://graph.facebook.com/v2.8/me"
            + "?fields=id,name,first_name,last_name,gender,email,picture,birthday";


    // set authentication for login and logout
    @GetMapping("/login")
    public String login(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!auth.getPrincipal().equals("anonymousUser")) {
            System.out.println(auth.getPrincipal());
            return "redirect:/";
        }
        model.addAttribute("accKitReq", new AccountKitPostRequest());
        return "user/login";
    }


    @GetMapping("/facebook/signin")
    public void signin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String secretState = "secret" + new Random().nextInt(999_999);
        request.getSession().setAttribute("SECRET_STATE", secretState);

        @SuppressWarnings("deprecation")
        OAuth20Service service = new ServiceBuilder()
                .apiKey(FB_APP_ID)
                .apiSecret(FB_APP_SECRET)
                .callback(DOMAIN + CALLBACK_URL)
                .scope(String.join(",", SCOPES))
                .state(secretState)
                .build(FacebookApi.instance());

        String authorizeUrl = service.getAuthorizationUrl();
        response.sendRedirect(authorizeUrl);
    }

    @GetMapping(value = "/facebook/callback")
    public String callback(@RequestParam(value = "code", required = false) String code,
                           @RequestParam(value = "state", required = false) String state,
                           HttpServletRequest request,
                           HttpServletResponse response) {
        try {
            @SuppressWarnings("deprecation")
            OAuth20Service service = new ServiceBuilder()
                    .apiKey(FB_APP_ID)
                    .apiSecret(FB_APP_SECRET)
                    .callback(DOMAIN + CALLBACK_URL)
                    .build(FacebookApi.instance());

            final String requestUrl = USER_PROFILE_API_URL;
            final OAuth2AccessToken accessToken = service.getAccessToken(code);
            final OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, requestUrl);
            service.signRequest(accessToken, oauthRequest);
            final Response resourceResponse = service.execute(oauthRequest);
            final JSONObject obj = new JSONObject(resourceResponse.getBody());
            request.getSession().setAttribute("FACEBOOK_ACCESS_TOKEN", accessToken);
            System.out.println("OBJ" + obj.toString());

            // Sign up new user and create login session
            User user = new User();
            // Try to set email if user allow email access
            try {
                user.setEmail(obj.getString("email"));
            } catch (Exception e) {
                user.setEmail("");
            }
            // Try to set gender if user allow gender access
            try {
                user.setGender(obj.getString("gender"));
            } catch (Exception e) {
                user.setGender("");
            }

            try {
                user.setName(obj.getString("name"));
            } catch (Exception e) {
                user.setName("");
            }

            try {
                user.setDateofbirth(obj.getString("birthday"));
            } catch (Exception e) {
                user.setDateofbirth("");
            }

            try {
                user.setUserimageurl("http://graph.facebook.com/" + obj.getString("id") + "/picture?type=large");
            } catch (Exception e) {
                user.setUserimageurl("http://api2.khmeracademy.org/resources/upload/file/user/avatar.jpg");
            }

            user.setSc_fb_id(obj.getString("id"));
            user.setPassword("");
            user.setPhonenumber("");
            user.setDepartmentid(12);
            user.setUniversityid(36);

            List<Role> roles = new ArrayList<>();
            roles.add(new Role()); // Add FACEBOOK_USER Role to user
            user.setRoles(roles);
            System.out.println("USER_ID: " + user.getSc_fb_id());
            Authentication auth;
            UserDetails getUser = userService.loadUserByUsername(user.getSc_fb_id());
            if (getUser != null) {
                System.out.println(getUser);
                System.out.println("register already");
                System.out.println("User:" + getUser.toString());
                auth = new UsernamePasswordAuthenticationToken(getUser, null, user.getRoles());
            } else {
                userService.userInsert(user);
                UserDetails findOneUser=userService.loadUserByUsername(user.getSc_fb_id());
                System.out.println("not yet register already");
                auth = new UsernamePasswordAuthenticationToken(findOneUser, null, user.getRoles());
            }

            //Create login session (manual login)
            SecurityContextHolder.getContext().setAuthentication(auth);
            String previousURL = (String) request.getSession().getAttribute("REDIRECT_SUCCESS_URL");

            System.out.println("Redirect: "+previousURL);
            if (previousURL == null) {
                return "redirect:/";
            }
            return "redirect:" + previousURL;
        } catch (Exception e) {
            e.printStackTrace();
            return "user/login";
        }
    }

    // for sms login
    // post mapping success redirect sms login
    @PostMapping("/login/redirect_success")
    public String loginSuccess(@ModelAttribute AccountKitPostRequest accKitReq,
                               HttpServletRequest request,
                               HttpServletResponse response) {
        String uri = TOKEN_EXCHANGE_BASE_URL + "?grant_type=authorization_code&code=" +
                accKitReq.getCode() + "&access_token=AA|" + FB_AK_APP_ID + "|" + AK_APP_SECRET;
        Object obj = restTemplate.getForObject(uri, Object.class);
        @SuppressWarnings("unchecked")
        Map<String, Object> info = (HashMap<String, Object>) obj;
        String phone_id = (String) info.get("id");
        String user_access_token = (String) info.get("access_token");

        String me_endpoint_uri = ME_ENDPOINT_BASE_URL + "?access_token=" + user_access_token;
        System.out.println(me_endpoint_uri);
        Object me = restTemplate.getForObject(me_endpoint_uri, Object.class);

        //Set Phone User Object
        @SuppressWarnings("unchecked")
        Map<String, Object> map = (HashMap<String, Object>) me;
        @SuppressWarnings("unchecked")
        Map<String, Object> phone = (HashMap<String, Object>) map.get("phone");
        String number = "0" + phone.get("national_number");

        User user = new User();
//        UserDetails getUser=userService.loadUserByUsername(phone_id);
        Authentication auth = null;
        UserDetails getUser = userService.loadUserByUsername(phone_id);
        if (getUser != null) {
            auth = new UsernamePasswordAuthenticationToken(getUser, null, null);
            SecurityContextHolder.getContext().setAuthentication(auth);
            //Create login session (manual login)
//            SecurityContextHolder.getContext().setAuthentication(auth);
            String previousURL = (String) request.getSession().getAttribute("REDIRECT_SUCCESS_URL");

            System.out.println("Redirect: "+previousURL);
            if (previousURL == null) {
                System.out.println("Home Page");
                return "redirect:/";
            }
            return "redirect:" + previousURL;
//            return "redirect:/";
        }

        // Sign up user and login
        user.setSc_fb_id(phone_id);
        user.setPhonenumber(number);
        auth = new UsernamePasswordAuthenticationToken(user, null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/user-information";
    }


    @GetMapping("/user-information")
    public String userInformation(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("departments", departmentService.findAllDepartment());
        model.addAttribute("universities", universiryService.findAllUniversity());
        return "user/register-info";
    }

    @PostMapping("/submit/information")
    public String register(@Valid @ModelAttribute User user, BindingResult result, ModelMap model) {
//       get old session
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User u = (User) auth.getPrincipal();
        user.setPhonenumber(u.getPhonenumber());
        user.setSc_fb_id(u.getSc_fb_id());
        user.setPassword("");
        user.setUserimageurl("/img/static-profile.png");


        if (result.hasErrors()) {
            model.addAttribute("departments", departmentService.findAllDepartment());
            model.addAttribute("universities", universiryService.findAllUniversity());
            return "user/register-info";
        }
        userService.userInsert(user);

        UserDetails getUser=userService.loadUserByUsername(user.getSc_fb_id());
        // clear old sesion and then set new use with user
        auth = new UsernamePasswordAuthenticationToken(getUser, null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        System.out.println("session: "+auth.getPrincipal());
        return "redirect:/";
    }

}
