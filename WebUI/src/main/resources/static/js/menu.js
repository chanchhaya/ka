$(document)
    .ready(function () {

        function myFunction(x) {
            if (x.matches) { // If media query matches
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function () {
                            $('.fixed.menu').transition('fade in');
                        },
                        onBottomPassedReverse: function () {
                            $('.fixed.menu').transition('fade out');
                        }
                    });
            } else {
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function () {
                            $('.fixed.menu').transition('fade out', '0ms');
                        },
                        onBottomPassedReverse: function () {
                            $('.fixed.menu').transition('fade out', '0ms');
                        }
                    });
            }
        }
        // fix menu when passed
        var x = window.matchMedia("(min-width: 991px)")
        myFunction(x) // Call listener function at run time
        x.addListener(myFunction) // Attach listener function on state changes
    });