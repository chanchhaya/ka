
function add(){
//    var name=document.getElementById('name').value;
//    var school=document.getElementById('school').value;
//    var contact=document.getElementById('contact').value;
//    var gender=document.getElementById('gender').value;
    var email=document.getElementById('email').value;
    validateEmail(email);
//    validateName(name);
//    validateSchool(school);
//    validateContact(contact);
}


function onChangeValidateName(){
    var name=document.getElementById('name').value;
    validateName(name);
}

function onChangeValidatePhoneNumber(){
    var phonenumber=document.getElementById('phonenumber').value;
    validateContact(phonenumber);
}

function validateName(name){
    var eName=document.getElementById("error-name");
    var num=/[0-9]/g;
    if(name==""||name==null){
       eName.innerHTML="<i class='fas fa-exclamation-triangle'> </i> *Requirement";
       return false;
    }
    if(num.test(name)){
        eName.innerHTML="<i class='fas fa-exclamation-triangle'> </i>*only character";
        return false;
    }else{
        eName.innerHTML="";
        return true;
    }
}

function validateContact(contact){
    var eContact=document.getElementById("error-phonenumber");
    var char=/[a-z A-Z]/g;
    var lengthContact=contact.length;
    var ws=/\s/g;
    if(contact==""||contact==null){
        eContact.innerHTML="<i class='fas fa-exclamation-triangle'> </i>*Requirement";
        return false;
    }else if(lengthContact<9 || lengthContact>10){
        eContact.innerHTML="<i class='fas fa-exclamation-triangle'> </i>*Max Langth is 9!";
        return false;
    }else if(char.test(contact)||ws.test(contact)){
        eContact.innerHTML="<i class='fas fa-exclamation-triangle'> </i>*Number only and without Space!";
        return false;
    }else{
        eContact.innerHTML="";
        return true;
    }
}


