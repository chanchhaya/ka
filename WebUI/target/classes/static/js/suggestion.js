var iconLoading = '<center><img src="https://oleahotel.com/wp-content/themes/q4fw/dist/images/loader-sm.gif" style="margin: auto;text-align: center;"/></center>';

$(document).ready(function () {
    var hostname='http://localhost:8080';



    //Search Suggestion
    $(window).click(function () {
        $('.menu-item').hide();
    })
    $('.sb-search-input').keyup(function () {
        var name = $('.sb-search-input').val();
        if (!name==""){
            $('.menu-item').show(100);
            searchSuggestion(name);
        }
    })
    $(window).scroll(function () {
        $('.menu-item').hide(100);
    })
//End Search Suggestion


//    get data from api
    function searchSuggestion(name) {
        var item='';
        $('.menu-item ul li').append(iconLoading);
        $.ajax({
            url: hostname+"/api/v1/suggestion?name="+''+name,
            type: "GET",
            success: function (data) {

                // alert("status: "+data.status);
                console.log(data.data.name);
                if(data.status==true){
                    // alert("true")
                    $(".menu-item ul").empty();
                    data.data.forEach(function (d) {
                        item+=`<li><a href="/elearning/video/watch?playlistid=`+d.encryptId+`&youtubeid=`+d.youtubeId+` ">`+d.name+`</a></li>`;
                    })

                    $('.menu-item ul').append(item);
                }else {
                    alert("false")
                }
            },
            error: function (err) {
                alert("Error")
                console.log(err)
            }
        })
    }

})

