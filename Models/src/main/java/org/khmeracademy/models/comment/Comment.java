package org.khmeracademy.models.comment;

import java.sql.Timestamp;

public class Comment {
    int commentId;
    int totalReply;
    String commentText;
    Timestamp commentDate;
    int userId;
    String userName;
    String userImageUrl;
    int videoId;
    int replycomId;
    int playlistId;

    public Comment(int commentId, int totalReply, String commentText, Timestamp commentDate, int userId, String userName, String userImageUrl, int videoId, int replycomId, int playlistId) {
        this.commentId = commentId;
        this.totalReply = totalReply;
        this.commentText = commentText;
        this.commentDate = commentDate;
        this.userId = userId;
        this.userName = userName;
        this.userImageUrl = userImageUrl;
        this.videoId = videoId;
        this.replycomId = replycomId;
        this.playlistId = playlistId;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public int getReplycomId() {
        return replycomId;
    }

    public void setReplycomId(int replycomId) {
        this.replycomId = replycomId;
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistid(int playlistId) {
        this.playlistId = playlistId;
    }

    public Comment() {
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getTotalReply() {
        return totalReply;
    }

    public void setTotalReply(int totalReply) {
        this.totalReply = totalReply;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Timestamp commentDate) {
        this.commentDate = commentDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }
}
