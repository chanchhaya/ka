package org.khmeracademy.models;

public class ListVideo {
    private String playlistname;
    private String playlistnamekh;
    private String videoname;
    private String youtubeid;
    private Integer index;

    public ListVideo() {
    }

    public ListVideo(String videoname, String youtubeid, Integer index, String playlistname, String playlistnamekh) {
        this.videoname = videoname;
        this.youtubeid = youtubeid;
        this.index = index;
        this.playlistname = playlistname;
        this.playlistnamekh = playlistnamekh;
    }

    public String getPlaylistname() {
        return playlistname;
    }

    public void setPlaylistname(String playlistname) {
        this.playlistname = playlistname;
    }

    public String getPlaylistnamekh() {
        return playlistnamekh;
    }

    public void setPlaylistnamekh(String playlistnamekh) {
        this.playlistnamekh = playlistnamekh;
    }

    public String getVideoname() {
        return videoname;
    }

    public void setVideoname(String videoname) {
        this.videoname = videoname;
    }

    public String getYoutubeid() {
        return youtubeid;
    }

    public void setYoutubeid(String youtubeid) {
        this.youtubeid = youtubeid;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

}
