package org.khmeracademy.models;

import java.sql.Timestamp;

public class Video {
    private Integer id;
    private String videoname;
    private Integer viewcount;
    private Timestamp postdate;
    private Long countLikes;

    public Video(Integer id, String videoname, Integer viewcount, Timestamp postdate, Long countLikes) {
        this.id = id;
        this.videoname = videoname;
        this.viewcount = viewcount;
        this.postdate = postdate;
        this.countLikes = countLikes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVideoname() {
        return videoname;
    }

    public void setVideoname(String videoname) {
        this.videoname = videoname;
    }

    public Integer getViewcount() {
        return viewcount;
    }

    public void setViewcount(Integer viewcount) {
        this.viewcount = viewcount;
    }

    public Timestamp getPostdate() {
        return postdate;
    }

    public void setPostdate(Timestamp postdate) {
        this.postdate = postdate;
    }

    public Long getCountLikes() {
        return countLikes;
    }

    public void setCountLikes(Long countLikes) {
        this.countLikes = countLikes;
    }
}
