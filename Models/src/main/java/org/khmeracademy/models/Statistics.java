package org.khmeracademy.models;

public class Statistics {
    private Integer users;
    private Integer videos;
    private Integer playlists;
    private Integer iosusers;
    private Integer aosusers;
    private Integer webusers;

    public Statistics(Integer users, Integer videos, Integer playlists, Integer iosusers, Integer aosusers, Integer webusers) {
        this.users = users;
        this.videos = videos;
        this.playlists = playlists;
        this.iosusers = iosusers;
        this.aosusers = aosusers;
        this.webusers = webusers;
    }

    public Integer getUsers() {
        return users;
    }

    public void setUsers(Integer users) {
        this.users = users;
    }

    public Integer getVideos() {
        return videos;
    }

    public void setVideos(Integer videos) {
        this.videos = videos;
    }

    public Integer getPlaylists() {
        return playlists;
    }

    public void setPlaylists(Integer playlists) {
        this.playlists = playlists;
    }

    public Integer getIosusers() {
        return iosusers;
    }

    public void setIosusers(Integer iosusers) {
        this.iosusers = iosusers;
    }

    public Integer getAosusers() {
        return aosusers;
    }

    public void setAosusers(Integer aosusers) {
        this.aosusers = aosusers;
    }

    public Integer getWebusers() {
        return webusers;
    }

    public void setWebusers(Integer webusers) {
        this.webusers = webusers;
    }
}
