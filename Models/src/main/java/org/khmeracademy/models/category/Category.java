package org.khmeracademy.models.category;
import org.khmeracademy.models.util.Encryption;

import java.util.List;

public class Category {
    private String id;
    private String name;
    private String bgimage;
    private String color;
    private String name_kh;
    private Integer sub_of;
    private String encriptId;

    private List<Category> subCategories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBgimage() {
        return bgimage;
    }

    public void setBgimage(String bgimage) {
        this.bgimage = bgimage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName_kh() {
        return name_kh;
    }

    public void setName_kh(String name_kh) {
        this.name_kh = name_kh;
    }

    public Integer getSub_of() {
        return sub_of;
    }

    public void setSub_of(Integer sub_of) {
        this.sub_of = sub_of;
    }

    public String getEncriptId() {
        return Encryption.encode(id);
    }

    public void setEncriptId(String encriptId) {
        this.encriptId = encriptId;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        this.subCategories = subCategories;
    }

}
