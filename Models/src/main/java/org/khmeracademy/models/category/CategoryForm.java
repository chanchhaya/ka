package org.khmeracademy.models.category;

import org.khmeracademy.models.util.Encryption;

public class CategoryForm {
    private int id;

    public CategoryForm() {
    }

    private String khName;
    private String enName;
    private int subOf;
    private String bgImage;
    private String color;
    private String encriptId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKhName() {
        return khName;
    }

    public void setKhName(String khName) {
        this.khName = khName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public int getSubOf() {
        return subOf;
    }

    public void setSubOf(int subOf) {
        this.subOf = subOf;
    }

    public String getBgImage() {
        return bgImage;
    }

    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEncriptId() {
        return Encryption.encode(id+"");
    }

    public void setEncriptId(String encriptId) {
        this.encriptId = encriptId;
    }
}
