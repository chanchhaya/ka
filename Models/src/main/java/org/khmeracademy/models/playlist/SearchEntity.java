package org.khmeracademy.models.playlist;

import org.khmeracademy.models.util.Encryption;

public class SearchEntity {
    private Integer id;
    private String name;
    private String youtubeId;
    private String encryptId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getEncryptId() {
        return Encryption.encode(id+"");
    }

    public void setEncryptId(String encryptId) {
        this.encryptId = encryptId;
    }
}
