package org.khmeracademy.models.playlist;

import org.khmeracademy.models.util.Encryption;

public class Playlists {

    private String id;
    private String name;
    private String thumbnail;
    private String bgimage;
    private Integer countvideoplay;
    private String youtubeId;
    private String encryptId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getBgimage() {
        return bgimage;
    }

    public void setBgimage(String bgimage) {
        this.bgimage = bgimage;
    }

    public Integer getCountvideoplay() {
        return countvideoplay;
    }
    public void setCountvideoplay(Integer countvideoplay) {
        this.countvideoplay = countvideoplay;
    }


    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getEncryptId() {
        return Encryption.encode(id);
    }

    public void setEncryptId(String encryptId) {
        this.encryptId = encryptId;
    }

}
