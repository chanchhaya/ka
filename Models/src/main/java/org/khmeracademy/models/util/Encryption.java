package org.khmeracademy.models.util;

import javax.xml.bind.DatatypeConverter;

public class Encryption {


    public static String encode(String code) {
        String encoded = "";
        try {
            encoded=DatatypeConverter.printBase64Binary(code.getBytes());
        } catch (Exception e) {
        }
        return encoded;
    }

    public static String decode(String encoded) {
        String decoded = "";
        try {
            decoded = new String(DatatypeConverter.parseBase64Binary(encoded));
        } catch (Exception e) {
        }
        return decoded;
    }
}
