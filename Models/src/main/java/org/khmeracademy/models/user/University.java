package org.khmeracademy.models.user;

public class University {
    private String universityid;
    private String universityname;

    public University(){

    }

    public University(String universityid, String universityname ){
        this.universityid = universityid;
        this.universityname = universityname;
    }

    public String getUniversityid() {
        return universityid;
    }

    public void setUniversityid(String universityid) {
        this.universityid = universityid;
    }

    public String getUniversityname() {
        return universityname;
    }

    public void setUniversityname(String universityname) {
        this.universityname = universityname;
    }
}
