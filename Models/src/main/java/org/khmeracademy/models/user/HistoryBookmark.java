package org.khmeracademy.models.user;

import org.khmeracademy.models.util.Encryption;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryBookmark {
    private Integer id;
    private Integer videoid;
    private Integer userid;
    private Integer playlistid;
    private String videoname;
    private String viewcount;
    private String youtubeurl;
    private String encryptId;

    public HistoryBookmark(){

    }

    public HistoryBookmark(Integer id, Integer videoid, Integer userid, Integer playlistid, String videoname, String viewcount, String youtubeurl, Date date) {
        this.id = id;
        this.videoid = videoid;
        this.userid = userid;
        this.playlistid = playlistid;
        this.videoname = videoname;
        this.viewcount = viewcount;
        this.youtubeurl = youtubeurl;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVideoid() {
        return videoid;
    }

    public void setVideoid(Integer videoid) {
        this.videoid = videoid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getPlaylistid() {
        return playlistid;
    }

    public void setPlaylistid(Integer playlistid) {
        this.playlistid = playlistid;
    }

    public String getVideoname() {
        return videoname;
    }

    public void setVideoname(String videoname) {
        this.videoname = videoname;
    }

    public String getViewcount() {
        return viewcount;
    }

    public void setViewcount(String viewcount) {
        this.viewcount = viewcount;
    }

    public String getYoutubeurl() {
        return youtubeurl;
    }

    public void setYoutubeurl(String youtubeurl) {
        this.youtubeurl = youtubeurl;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;

    public String getEncryptId() {
        return Encryption.encode(""+playlistid);
    }

    public void setEncryptId(String encryptId) {
        this.encryptId = encryptId;
    }

}
