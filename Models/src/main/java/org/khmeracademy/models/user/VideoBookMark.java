package org.khmeracademy.models.user;

import java.util.Date;

public class VideoBookMark {

    private Integer videoid;

    private String videoname;
    private String description;
    private String youtubeurl;
    private String fileurl;
    private Boolean publicview;
    private String postdate;
    private Integer userid;
    private Integer viewcount;
    private Boolean status;
    private Integer duration;

    @Override
    public String toString() {
        return "VideoBookMark{" +
                "videoid=" + videoid +
                ", videoname='" + videoname + '\'' +
                ", description='" + description + '\'' +
                ", youtubeurl='" + youtubeurl + '\'' +
                ", fileurl='" + fileurl + '\'' +
                ", publicview=" + publicview +
                ", postdate='" + postdate + '\'' +
                ", userid=" + userid +
                ", viewcount=" + viewcount +
                ", status=" + status +
                ", duration=" + duration +
                '}';
    }

    public Integer getVideoid() {
        return videoid;
    }

    public void setVideoid(Integer videoid) {
        this.videoid = videoid;
    }

    public String getVideoname() {
        return videoname;
    }

    public void setVideoname(String videoname) {
        this.videoname = videoname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYoutubeurl() {
        return youtubeurl;
    }

    public void setYoutubeurl(String youtubeurl) {
        this.youtubeurl = youtubeurl;
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    public Boolean getPublicview() {
        return publicview;
    }

    public void setPublicview(Boolean publicview) {
        this.publicview = publicview;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getViewcount() {
        return viewcount;
    }

    public void setViewcount(Integer viewcount) {
        this.viewcount = viewcount;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
