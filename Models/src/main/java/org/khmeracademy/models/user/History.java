package org.khmeracademy.models.user;

public class History extends VideoBookMark {

    private Integer historyid;
    private String historydate;
    private Integer userid;
    private Integer videoid;

    @Override
    public String toString() {
        return "History{" +
                "historyid=" + historyid +
                ", historydate='" + historydate + '\'' +
                ", userid=" + userid +
                ", videoid=" + videoid +
                '}';
    }

    public Integer getHistoryid() {
        return historyid;
    }

    public void setHistoryid(Integer historyid) {
        this.historyid = historyid;
    }

    public String getHistorydate() {
        return historydate;
    }

    public void setHistorydate(String historydate) {
        this.historydate = historydate;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getVideoid() {
        return videoid;
    }

    public void setVideoid(Integer videoid) {
        this.videoid = videoid;
    }
}
