package org.khmeracademy.models.user;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public class User implements UserDetails {
    private String userid;
    private String email;
    private String password;
    @NotBlank
    @NotNull
    @NotEmpty
    private String name;
    @NotBlank
    private String gender;
    private String dateofbirth;
    private String phonenumber;
    private String userimageurl;
    @NotNull
    private Integer departmentid;
    @NotNull
    private Integer universityid;
    private Integer userstatus;
    private String sc_fb_id;
    private Integer signup_with;
    private List<Role> roles;

    @Override
    public String toString() {
        return "User{" +
                "userid='" + userid + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", dateofbirth='" + dateofbirth + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", userimageurl='" + userimageurl + '\'' +
                ", departmentid=" + departmentid +
                ", universityid=" + universityid +
                ", userstatus=" + userstatus +
                ", sc_fb_id='" + sc_fb_id + '\'' +
                ", signup_with=" + signup_with +
                ", roles=" + roles +
                '}';
    }

    public User(){}

    public User(String email, String password, String name, String gender, String phonenumber, String userimageurl, Integer departmentid, Integer universityid, String sc_fb_id) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.gender = gender;
        this.phonenumber = phonenumber;
        this.userimageurl = userimageurl;
        this.departmentid = departmentid;
        this.universityid = universityid;
        this.sc_fb_id = sc_fb_id;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getUserimageurl() {
        return userimageurl;
    }

    public void setUserimageurl(String userimageurl) {
        this.userimageurl = userimageurl;
    }

    public Integer getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        this.departmentid = departmentid;
    }

    public Integer getUniversityid() {
        return universityid;
    }

    public void setUniversityid(Integer universityid) {
        this.universityid = universityid;
    }

    public Integer getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(Integer userstatus) {
        this.userstatus = userstatus;
    }

    public String getSc_fb_id() {
        return sc_fb_id;
    }

    public void setSc_fb_id(String sc_fb_id) {
        this.sc_fb_id = sc_fb_id;
    }

    public Integer getSignup_with() {
        return signup_with;
    }

    public void setSignup_with(Integer signup_with) {
        this.signup_with = signup_with;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return sc_fb_id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
