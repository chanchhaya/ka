package org.khmeracademy.repositoy.comment;

import org.khmeracademy.models.comment.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    private CommentRepository commentRepository;
    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }
    public List<Comment> getComment(int videoId){
        return  commentRepository.getComment(videoId);
    }
    public List<Comment> getReplyComment(int commentId){
        return commentRepository.getReplyComment(commentId);
    }
    public int saveComment(Comment comment){
        return commentRepository.saveComment(comment);
    }
}
