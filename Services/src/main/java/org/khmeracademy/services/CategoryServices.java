package org.khmeracademy.services;

import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.category.Category;
import org.khmeracademy.repositoy.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class CategoryServices {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<CategoryForm> getcategorieslevel1(int id) {
        return this.categoryRepository.getcategorieslevel1(id);
    }

//    find section two home page
    public List<Category> findSectionTwo(){
        return categoryRepository.findSectionTwo();
    }
// find section three page
    public List<Category> findSectionThree(){
        return categoryRepository.findSectionThree();
    }
}
