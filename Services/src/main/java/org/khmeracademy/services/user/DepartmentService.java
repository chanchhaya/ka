package org.khmeracademy.services.user;

import org.khmeracademy.models.user.Department;
import org.khmeracademy.repositoy.login.DepartementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    private DepartementRepository departementRepository;

    @Autowired
    public void setDepartementRepository(DepartementRepository departementRepository) {
        this.departementRepository = departementRepository;
    }


    public List<Department> findAllDepartment(){
        return departementRepository.findAllDepartment();
    }
}
