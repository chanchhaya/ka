package org.khmeracademy.services.user;

import org.khmeracademy.models.user.University;
import org.khmeracademy.repositoy.login.UniversityRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniversiryService {

    private UniversityRespository universityRespository;

    @Autowired
    public void setUniversityRespository(UniversityRespository universityRespository) {
        this.universityRespository = universityRespository;
    }

    public List<University> findAllUniversity(){
        return universityRespository.findAllUniversity();
    }
}
