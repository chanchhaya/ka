package org.khmeracademy.services;

import org.khmeracademy.models.ListVideo;
import org.khmeracademy.models.Video;
import org.khmeracademy.repositoy.ListVideoRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;
import java.util.List;

@Service
public class ListVideoService {
    private ListVideoRepository listVideoRepository;

    public ListVideoService(ListVideoRepository listVideoRepository) {
        this.listVideoRepository = listVideoRepository;
    }

    public List<ListVideo> listVideo(@PathVariable("id") Integer id) {
        List<ListVideo> vdos = new ArrayList<>();
        List<ListVideo> videos = listVideoRepository.listvideos(id);
        for (ListVideo vdo : videos) {
            vdos.add(new ListVideo(vdo.getVideoname(), vdo.getYoutubeid(),vdo.getIndex(),vdo.getPlaylistname(),vdo.getPlaylistnamekh()));
        }
        return vdos;
    }

    public ListVideo findFirstYoutubeUrlForPlaylist(Integer playlistid){
        return listVideoRepository.findOneFirstYoutubeUrlForPlaylist(playlistid);
    }


    // count like, count view, title for video
    public Video video(String youtubeid) {
        return listVideoRepository.vdo(youtubeid);
    }
}
