package org.khmeracademy.services.playlist;

import org.khmeracademy.models.playlist.SearchEntity;
import org.khmeracademy.repositoy.playlist.SuggestionSearchRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuggestionSearchService {

    private SuggestionSearchRepositoy searchRepositoy;

    @Autowired
    public void setSearchRepositoy(SuggestionSearchRepositoy searchRepositoy) {
        this.searchRepositoy = searchRepositoy;
    }

    public List<SearchEntity> findAllSearchFiller(String name){
        return searchRepositoy.findAllSearchFiller(name);
    }

}
