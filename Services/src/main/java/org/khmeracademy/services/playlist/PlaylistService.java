package org.khmeracademy.services.playlist;

import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.playlist.Playlists;
import org.khmeracademy.repositoy.playlist.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaylistService {
    private PlaylistRepository playlistRepository;

    @Autowired
    public void setPlaylistRepository(PlaylistRepository playlistRepository) {
        this.playlistRepository = playlistRepository;
    }

    public List<Playlists> searchByPlaylistName(String name) {
        return playlistRepository.searchByPlaylistName(name);
    }

    public List<Playlists> getPlaylists(int  categoryId, Pagination pagination){
        return this.playlistRepository.getPlaylists(categoryId,pagination);
    }

    public Integer getAllPlaylists(int id) {
        return this.playlistRepository.getAllPlaylists(id);
    }

//    Test
    public List<Playlists> searchResultPlaylist(String name,Pagination pagination){
        return playlistRepository.searchResultPlaylist(name,pagination);
    }

    public Integer countAllPlaylistSearch(String name){
        return playlistRepository.countAllPlaylistSearch(name);
    }
 }
