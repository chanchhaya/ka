package org.khmeracademy.services;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.user.HistoryBookmark;
import org.khmeracademy.models.user.User;
import org.khmeracademy.repositoy.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return (UserDetails) userRepository.loadUserByFacebookId(s);
    }

    public void userInsert(User user) {
        userRepository.userInsert(user);
    }


    public User findOneUserByFbId(String id) {
        return userRepository.findOneUserByFbId(id);
    }

    public Boolean userUpdate(User user) {
        return userRepository.userUpdate(user);
    }

    public List<HistoryBookmark> userVideoBookMark(Integer userId, Pagination pagination){
        return userRepository.userVideoBookMark(userId,pagination);
    }

    public List<HistoryBookmark> userHistory(Integer  userId, Pagination pagination){
        return userRepository.userHistory(userId,pagination);
    };

    public Integer countVideoHistory(Integer userId){
        return  userRepository.countVideoHistory(userId);
    }

    public Boolean historyDelete(Integer historyid){
        return userRepository.historyDelete(historyid);
    }

    public Boolean bookmarkUpdate(Integer videoid,Boolean status){
        return userRepository.bookmarkUpdate(videoid,status);
    }

    public User findOneUserByUserId(String userid){
        return userRepository.findOneUserByUserId(userid);
    }
    public User findOneUserByFacebookID(String facebookid){
        return userRepository.findOneUserByFacebookID(facebookid);
    }

    public Integer countVideoBookmark(Integer userId){
        return userRepository.countVideoBookmark(userId);
    }

    public Boolean insertBookmark(Integer userId,Integer videoId){
        return userRepository.insertVideoBookmark(userId,videoId);
    }

    public Boolean checkBookmark(Integer userid,Integer videoid){
        return userRepository.checkBookmark(userid,videoid);
    }

    public Boolean insertHistory(Integer userid,Integer videoid){
        return userRepository.insertHistory(userid,videoid);
    }

}
