package org.khmeracademy.services;

import org.khmeracademy.models.Vote;
import org.khmeracademy.repositoy.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoteService {
    private VoteRepository voteRepository;

    @Autowired
    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public Vote findOneVoteVideoByUser(Integer userId, Integer videoId){
        return voteRepository.findOneVoteVideoByUser(userId, videoId);
    }

    public boolean updateVoteByStatusByUserID(Integer userId, Integer voteType, Integer videoId){
        return voteRepository.updateVoteByStatusByUserID(userId, voteType, videoId);
    }

    public boolean insertVote(Vote vote){
        return voteRepository.insertVote(vote);
    }
}
