package org.khmeracademy.services;

import org.khmeracademy.models.Statistics;
import org.khmeracademy.repositoy.StatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class StatisticServices {

    @Autowired
    private StatisticRepository statistic;

    public List<Statistics> statistics(){
        List <Statistics> st =new ArrayList<>();
        st.add(new Statistics(statistic.videos(),statistic.users(),statistic.playlists(),statistic.iosusers(),statistic.aosusers(),statistic.webusers()));
        return  st;
    }
}
