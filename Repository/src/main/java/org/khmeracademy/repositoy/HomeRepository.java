package org.khmeracademy.repositoy;


import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.category.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeRepository {

    @Select("SELECT * FROM findallcategorylevel1 WHERE maincategoryid=76 or maincategoryid=57 ORDER BY \"index\" DESC;")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "url", column = "maincategorylogourl"),
            @Result(property = "order", column = "maincategoryorder"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "des_kh", column = "description_kh"),
    })
    public List<Category> findHighAndSecondarySchool();



}
