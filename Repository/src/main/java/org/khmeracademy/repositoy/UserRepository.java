package org.khmeracademy.repositoy;

import org.apache.ibatis.annotations.*;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.user.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("select * from tbluser where sc_fb_id like #{id}")
    @Results({
            @Result(property = "userid",column = "userid"),
            @Result(property = "name",column = "username"),
            @Result(property = "roles",column = "id",many=@Many(select = "findRoleByUserId"))
    })
    User loadUserByFacebookId(String id);

    @Select("select r.role_id,r.role_name from tblrole r INNER JOIN tbluser_role ur ON ur.role_id=r.role_id WHERE ur.user_id=#{id}")
    List<Role> findRoleByUserId(int id);

    @Insert("SELECT save_user(#{email}, #{password}, #{name}, #{gender}, #{phonenumber}, #{userimageurl}, #{universityid}, #{departmentid}, #{sc_fb_id})")
    void userInsert(User user);

    @Update("UPDATE tbluser SET username =#{name},email=#{email},gender=#{gender},dateofbirth=CAST (#{dateofbirth} AS Date),phonenumber=#{phonenumber},departmentid=CAST (#{departmentid} AS integer),universityid=CAST (#{universityid} AS integer),userimageurl=#{userimageurl} where sc_fb_id like #{sc_fb_id}")
    @Results({
            @Result(property = "name",column = "username")
    })
    Boolean userUpdate(User user);

    @Select("select v.videoid,v.videoname,v.viewcount,v.youtubeurl,vm.created_date,pld.playlistid " +
            "from tblvideobookmark vm " +
            "INNER JOIN tblvideo v on v.videoid=vm.video_id " +
            "INNER JOIN tblplaylistdetail pld on pld.videoid=v.videoid "+
            "where vm.user_id=#{userId} and vm.status=true " +
            "ORDER BY vm.created_date DESC "+
            "limit #{pagination.limit} offset #{pagination.offset}")
    @Results({
            @Result(property = "date",column ="created_date")
    })
    List<HistoryBookmark> userVideoBookMark(@Param("userId") Integer userId, @Param("pagination") Pagination pagination);
//    edit
    @Select("SELECT * FROM viewhistories(#{userId},#{pagination.limit},#{pagination.offset})")

    List<HistoryBookmark> userHistory(@Param("userId") Integer userId, @Param("pagination") Pagination pagination);

    @Delete("delete from tblhistory h where h.videoid=#{videoid}")
    Boolean historyDelete(Integer videoid);

    @Update("update tblvideobookmark set status=#{status} where video_id=#{videoid}")
    Boolean bookmarkUpdate(@Param("videoid")Integer videoid,@Param("status")Boolean status);

    @Select("select * from tbluser where userid= CAST (#{userid} AS Integer)")
    @Results({
            @Result(property = "userid",column = "userid"),
            @Result(property = "name",column = "username"),
            @Result(property = "roles",column = "id",many=@Many(select = "findRoleByUserId"))
    })
    User findOneUserByUserId(String userid);

    @Select("select * from tbluser where sc_fb_id like #{facebookid}")
    User findOneUserByFacebookID(String facebookid);

    @Select("select * from tbluser where s")
    public User findOneUserByFbId(String id);

    @Select("select count(vbm.user_id) videohistorycount " +
            "from tblvideobookmark vbm " +
            "INNER JOIN tblvideo v on v.videoid=vbm.video_id " +
            "where vbm.user_id=#{userId} " +
            "AND vbm.status='t'")
    public Integer countVideoBookmark(Integer userId);

    @Select("select count(h.historyid) videohistorycount "+
            "from tblhistory h " +
            "INNER JOIN tblvideo v on v.videoid=h.videoid " +
            "where h.userid=#{userId}")
    public Integer countVideoHistory(Integer userId);

//    @Select("select insertbookmark(#{facebookId},#{urlVideo})")
//    public Boolean insertVideoBookmark(@Param("facebookId") String facebookId,@Param("urlVideo") String urlVideo);

    @Insert("Insert Into tblvideobookmark (user_id,video_id) VALUES(#{userId},#{videoId})")
    public Boolean insertVideoBookmark(@Param("userId")Integer userId,@Param("videoId")Integer videoId);
    @Select("select vbm.status from tblvideobookmark vbm where vbm.user_id=#{userid} and vbm.video_id=#{videoid}")
    public Boolean checkBookmark(@Param("userid")Integer userid,@Param("videoid")Integer videoid);

    @Insert("INSERT INTO tblhistory(userid,videoid) " +
            "Values(#{userid},#{videoid})")
    public Boolean insertHistory(@Param("userid")Integer userid,@Param("videoid")Integer videoid);


}
