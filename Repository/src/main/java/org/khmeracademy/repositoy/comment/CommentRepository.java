package org.khmeracademy.repositoy.comment;

import org.apache.ibatis.annotations.*;
import org.khmeracademy.models.comment.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {

    @Select("SELECT * FROM comment_level1(#{videoId})")
    @Results({
            @Result(property ="commentId",column = "commentid"),
            @Result(property ="totalReply",column = "countreply"),
            @Result(property ="commentText",column = "commenttext"),
            @Result(property ="commentDate",column = "commentdate"),
            @Result(property ="userName",column = "username"),
            @Result(property ="userId",column = "userid"),
            @Result(property ="userImageUrl",column = "userimageurl")
    })
    List<Comment> getComment(int videoId);

    @Select("SELECT * FROM reply(#{commentId})")
    @Results({
            @Result(property ="commentId",column = "commentid"),
            @Result(property ="commentText",column = "commenttext"),
            @Result(property ="commentDate",column = "commentdate"),
            @Result(property ="userName",column = "username"),
            @Result(property ="userId",column = "userid"),
            @Result(property ="userImageUrl",column = "userimageurl")
    })
    List<Comment> getReplyComment(int commentId);

    @Insert("insert into tblcomment (commentdate,commenttext,videoid,userid,replycomid,playlistid) " +
            "values (now(),#{comment.commentText},#{comment.videoId},#{comment.userId},#{comment.replycomId},#{comment.playlistId})")
    public int saveComment(@Param("comment") Comment comment);
}
