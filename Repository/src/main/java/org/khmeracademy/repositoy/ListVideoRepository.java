package org.khmeracademy.repositoy;

import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.ListVideo;
import org.khmeracademy.models.Video;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListVideoRepository {

    @Select("SELECT * FROM videosplaylists(#{id})")
    List<ListVideo> listvideos(Integer id);

    @Select("SELECT * from view_playlist_youtube l where l.playlistid=#{playlistid} limit 1")
    public ListVideo findOneFirstYoutubeUrlForPlaylist(Integer playlistid);

    @Select("SELECT * FROM video(#{youtube})")
    Video vdo(String youtube);

}
