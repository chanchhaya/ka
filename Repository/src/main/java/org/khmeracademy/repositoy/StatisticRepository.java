package org.khmeracademy.repositoy;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


@Repository
public interface StatisticRepository {

    @Select("select * from videocounts")
//    @Results(@Result(property = "videos",column = "videos"))
    Integer videos();

    @Select("select * from usercounts")
//    @Results(@Result(property = "users",column = "totalusers"))
    Integer users();

    @Select("select * from playlistcounts")
//    @Results(@Result(property = "playlists",column = "tolalplaylist"))
    Integer playlists();

    @Select("SELECT * FROM IOSUSERS")
    Integer iosusers();

    @Select("SELECT * FROM AOSUSERS")
    Integer aosusers();

    @Select("SELECT * FROM WEBUSERS")
    Integer webusers();
}
