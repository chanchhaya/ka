package org.khmeracademy.repositoy;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.category.Category;
import org.khmeracademy.models.playlist.Playlists;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT * From categorystatustrue(#{id})")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "khName", column = "maincategoryname_kh"),
            @Result(property = "enName", column = "maincategoryname"),
            @Result(property = "subOf", column = "sub_of")
    })
    public List<CategoryForm> getcategorieslevel1(int id);

//    Final Fragments
//    Section One

    @Select("SELECT * FROM findallcategorylevel1 WHERE maincategoryid IN(57,76);")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
    })
    public List<Category> findHighAndSecondary();


//    Section Two
    @Select("SELECT * FROM findallcategorylevel1 WHERE maincategoryid NOT IN(57,76,55,67);")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findSubCategoryTwo"))
    })
    public List<Category> findSectionTwo();

    @Select("SELECT \n" +
            "m.maincategoryid,\n" +
            "m.maincategoryname,\n" +
            "m.bgimage,\n" +
            "m.maincategoryname_kh,\n" +
            "m.sub_of\n" +
            "FROM tblmaincategory m where m.sub_of=#{maincategoryid} AND m.status=TRUE ORDER BY index ASC")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findSubPlaylistTwo"))
    })
    public List<Category> findSubCategoryTwo(Integer id);

    @Select("SELECT\n" +
            "vpl.playlistid,\n" +
            "vpl.playlistname,\n" +
            "vpl.bgimage,\n" +
            "vpl.thumbnailurl,\n" +
            "vpl.totalcount\n" +
            "FROM view_playlist vpl WHERE vpl.maincategory=#{maincategoryid}  ORDER BY vpl.INDEX DESC LIMIT 4;")
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "thumbnail", column = "thumbnailurl"),
            @Result(property = "bgimage", column = "bgimage"),
            @Result(property = "countvideoplay", column = "totalcount"),
            @Result(property = "youtubeId", column = "playlistid", many = @Many(select = "findOneYoutubeUrl"))
    })
    public List<Playlists> findSubPlaylistTwo(Integer maincategoryid);

    @Select("SELECT * from view_playlist_youtube l where l.playlistid=#{id} limit 1;")
    @Results({
            @Result(property = "youtubeId", column = "youtubeurl")
    })
    public String findOneYoutubeUrl(Integer id);

//    section Three

    @Select("SELECT * FROM findallcategorylevel1 WHERE maincategoryid IN(55,67)")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findSubCategoryTwo"))
    })
    public List<Category> findSectionThree();

    @Select("SELECT \n" +
            "m.maincategoryid,\n" +
            "m.maincategoryname,\n" +
            "m.bgimage,\n" +
            "m.maincategoryname_kh,\n" +
            "m.sub_of\n" +
            "FROM tblmaincategory m where m.sub_of=#{maincategoryid} AND m.status=TRUE ORDER BY index ASC")
    @Results({
            @Result(property = "id", column = "maincategoryid"),
            @Result(property = "name", column = "maincategoryname"),
            @Result(property = "name_kh", column = "maincategoryname_kh"),
            @Result(property = "subCategories", column = "maincategoryid", many = @Many(select = "findSubPlaylistThree"))
    })
    public List<Category> findSubSectionThree(Integer id);


    @Select("SELECT\n" +
            "vpl.playlistid,\n" +
            "vpl.playlistname,\n" +
            "vpl.bgimage,\n" +
            "vpl.thumbnailurl,\n" +
            "vpl.totalcount\n" +
            "FROM view_playlist vpl WHERE vpl.maincategory=#{maincategoryid}  ORDER BY vpl.INDEX DESC LIMIT 4;")
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "thumbnail", column = "thumbnailurl"),
            @Result(property = "bgimage", column = "bgimage"),
            @Result(property = "countvideoplay", column = "totalcount"),
            @Result(property = "youtubeId", column = "playlistid", many = @Many(select = "findUrlYoutube"))
    })

    public List<Playlists> findSubPlaylistThree(Integer id);


    @Select("SELECT * from view_playlist_youtube l where l.playlistid=#{id} limit 1;")
    @Results({
            @Result(property = "youtubeId", column = "youtubeurl")
    })
    public String findUrlYoutube(Integer id);


}
