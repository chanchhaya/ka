package org.khmeracademy.repositoy;

import org.khmeracademy.models.Pagination;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaginationRepository {
    public List<Pagination> allPlaylist();
}
