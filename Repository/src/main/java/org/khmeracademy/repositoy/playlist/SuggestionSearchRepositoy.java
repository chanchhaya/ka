package org.khmeracademy.repositoy.playlist;

import org.apache.ibatis.annotations.*;
import org.khmeracademy.models.playlist.SearchEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SuggestionSearchRepositoy {

    @Select("SELECT \tpl.playlistid, \n" +
            "\t\t\t\tpl.playlistname\n" +
            "\t\t\t\tFROM \n" +
            "tblplaylist pl \n" +
            "WHERE pl.playlistname ILIKE '' || #{name} || '%'\n" +
            "AND pl.status=TRUE \n" +
            "ORDER BY pl.playlistname  ASC;")
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "youtubeId", column = "playlistid", many = @Many(select = "findOneYoutubeUrl"))
    })

    public List<SearchEntity> findAllSearchFiller(@Param("name") String name);


    @Select("SELECT * from view_playlist_youtube l where l.playlistid=#{id} limit 1;")
    @Results({
            @Result(property = "youtubeId", column = "youtubeurl")
    })
    public String findOneYoutubeUrl(Integer id);


}
