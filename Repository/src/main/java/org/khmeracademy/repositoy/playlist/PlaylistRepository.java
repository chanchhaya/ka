package org.khmeracademy.repositoy.playlist;

import org.apache.ibatis.annotations.*;
import org.khmeracademy.models.playlist.Playlists;
import org.khmeracademy.repositoy.provider.PlaylistProvider;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.category.CategoryForm;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistRepository {
    @SelectProvider(method = "searchPlaylistProvider", type = PlaylistProvider.class)
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "thumbnail", column = "thumbnailurl"),
            @Result(property = "bgimage", column = "bgimage"),
            @Result(property = "countvideoplay", column = "totalcount")
    })
    public List<Playlists> searchByPlaylistName(String name);


    @Select("SELECT " +
            "pl.playlistid," +
            "pl.playlistname," +
            "pl.thumbnailurl," +
            "pl.bgimage," +
            "pl.totalcount " +
            "FROM view_playlist pl " +
            "WHERE playlistname ILIKE '%'|| #{name} ||'%' " +
            "LIMIT #{pagination.limit} " +
            "OFFSET #{pagination.offset};")
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "thumbnail", column = "thumbnailurl"),
            @Result(property = "bgimage", column = "bgimage"),
            @Result(property = "countvideoplay", column = "totalcount")
    })
    public List<Playlists> searchResultPlaylist(@Param("name") String name,@Param("pagination") Pagination pagination);


    @Select("SELECT COUNT(playlistid) FROM view_playlist WHERE playlistname ILIKE '%' || #{name} || '%';")
    public Integer countAllPlaylistSearch(String name);



    @Select("SELECT * FROM getplayLists(#{categoryId}, #{pagination.limit}, #{pagination.offset})")
    @Results({
            @Result(property = "id", column = "playlistid"),
            @Result(property = "name", column = "playlistname"),
            @Result(property = "bgimage", column = "thumbnailurl"),
            @Result(property = "countvideoplay", column = "videoscounts"),
            @Result(property = "youtubeId", column = "playlistid", many = @Many(select = "findOneYoutubeUrl"))
    })
    public List<Playlists> getPlaylists(@Param("categoryId") int categoryId, @Param("pagination") Pagination pagination);


    @Select("SELECT * from view_playlist_youtube l where l.playlistid=#{id} limit 1;")
    @Results({
            @Result(property = "youtubeId", column = "youtubeurl")
    })
    public String findOneYoutubeUrl(Integer id);



    @Select("select count(playlistid) videoscounts from  tblplaylist where  maincategory=#{id}")
    @Results({
            @Result(property = "countvideoplay", column = "videoscounts")
    })
    public Integer getAllPlaylists(int id);



}
