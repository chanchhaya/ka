package org.khmeracademy.repositoy.provider;

import org.apache.ibatis.jdbc.SQL;

public class PlaylistProvider {
    public String searchPlaylistProvider(String name) {
        return new SQL() {{
            SELECT("playlistid, playlistname, bgimage, thumbnailurl, totalcount");
            FROM("view_playlist");
            if (name != null) {
                WHERE("playlistname ILIKE '%' || #{name} || '%'");
            }

        }}.toString();
    }
}
