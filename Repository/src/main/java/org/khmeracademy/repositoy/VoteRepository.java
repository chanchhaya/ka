package org.khmeracademy.repositoy;


import org.apache.ibatis.annotations.*;
import org.khmeracademy.models.Vote;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository {

    @Select("SELECT * FROM tblvote \n" +
            "WHERE userid = #{userId}\n" +
            "AND videoid = #{videoId}")
    public Vote findOneVoteVideoByUser(@Param("userId") Integer userId, @Param("videoId") Integer videoId);

    @Update("UPDATE tblvote SET votetype=#{voteType} WHERE userid=#{userId} AND videoid=#{videoId}")
    public boolean updateVoteByStatusByUserID(
            @Param("userId") Integer userId,
            @Param("voteType") Integer voteType,
            @Param("videoId") Integer videoId
    );

    @Insert("INSERT INTO tblvote(userid, videoid, votetype) VALUES(#{userId},#{videoId},#{voteType})")
    public boolean insertVote(Vote vote);

}

