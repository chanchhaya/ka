package org.khmeracademy.repositoy.login;

import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.user.Department;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartementRepository {

    @Select("select departmentid, departmentname from tbldepartment order by index")
    public List<Department> findAllDepartment();

}
