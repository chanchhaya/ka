package org.khmeracademy.repositoy.login;

import org.apache.ibatis.annotations.Select;
import org.khmeracademy.models.user.University;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UniversityRespository {

    @Select("select universityid, universityname from tbluniversity order by index")
    public List<University> findAllUniversity();
}
