package org.khmeracademy.webServices.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.user.HistoryBookmark;
import org.khmeracademy.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "BookMark Api ", description = "BookMark Video", tags = "BookMark Api")
public class BookMarkWebservice {

    private UserServiceImpl userService;

    @Autowired
    public BookMarkWebservice(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/videobookmark/{userId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", dataType = "int", paramType = "query", required = false, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "limit", dataType = "int", paramType = "query", required = false, defaultValue = "12")
    })
    public ResponseEntity<Map<String,Object>> findAllVideoBookMark(@PathVariable("userId") Integer userId, @ApiIgnore Pagination pagination){
        List<HistoryBookmark> videoBookMarkList=new ArrayList<>();
        Map<String,Object> responce=new HashMap<>();
        videoBookMarkList=userService.userVideoBookMark(userId,pagination);
        if(videoBookMarkList.isEmpty()){

            responce.put("status","false");
            responce.put("message","Video BookMark not found");
            return  new ResponseEntity(responce, HttpStatus.NOT_FOUND);
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        responce.put("data", videoBookMarkList);
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @PutMapping("/videobookmark/update/{id}/{status}")
    public ResponseEntity<Map<String,Object>> deleteVideobookmark(@PathVariable("id")int id,@PathVariable("status")Boolean status){
        Boolean videoBookMarkDelete;
        Map<String,Object> responce=new HashMap<>();
        videoBookMarkDelete=userService.bookmarkUpdate(id,status);
        System.out.println("VideoBookmarkDelete:"+videoBookMarkDelete);
        if(videoBookMarkDelete==false){
            responce.put("status","false");
            responce.put("message","Video BookMark Not Found");
            return new ResponseEntity(responce,HttpStatus.NOT_FOUND );
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @GetMapping("/countvideobookmark/{userId}")
    public ResponseEntity<Map<String, Object>> countVideoBookmark(@PathVariable("userId") Integer userId) {
        Map<String, Object> responce = new HashMap<>();
        Integer countVideoBookmark = userService.countVideoBookmark(userId);
        System.out.println("countVideoBookmark:"+countVideoBookmark);
        if (countVideoBookmark == 0) {
            responce.put("status", "false");
            responce.put("message", "Video BookMark Not Found");
            return new ResponseEntity(responce, HttpStatus.NOT_FOUND);
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        responce.put("data", countVideoBookmark);
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @PostMapping("/videobookmark/insert/{userId}/{videoId}")
    public ResponseEntity<Map<String,Object>> insertVideoBookmark(@PathVariable("userId")Integer userId,@PathVariable("videoId")Integer videoId){
        Map<String,Object> responce=new HashMap<>();
        System.out.println("facebookid:"+videoId);
        System.out.println("Videoid:"+videoId);
        Boolean check=userService.insertBookmark(userId,videoId);
        System.out.println("Check:"+check);

        if(check==true){
            responce.put("status","true");
            responce.put("message","Success");
            responce.put("data",check);
        }else{
            responce.put("status","false");
            responce.put("message","Video BookMark Not Found");
            return new ResponseEntity(responce,HttpStatus.NOT_FOUND );
        }
        return new ResponseEntity(responce,HttpStatus.OK);
    }
    @GetMapping("/videobookmark/check/{userid}/{videoid}")
    public ResponseEntity<Map<String,Object>> insertVideoBookmark2(@PathVariable("userid")Integer userid,@PathVariable("videoid")Integer videoid){
        Map<String,Object> responce=new HashMap<>();
        Boolean check=userService.checkBookmark(userid,videoid);
        String result=null;
        System.out.println("Check:"+check);
        System.out.println("Result:"+result);
        if(check==null){
            responce.put("status","true");
            responce.put("message","Success");
            responce.put("data",result);
        }else if(check==true) {
            responce.put("status", "true");
            responce.put("message", "Success");
            responce.put("data", check);
        }else if(check==false){
            responce.put("status","true");
            responce.put("message","Success");
            responce.put("data",check);
        }else{
            responce.put("status","false");
            responce.put("message","Video BookMark Not Found");
            return new ResponseEntity(responce,HttpStatus.NOT_FOUND );
        }
        return new ResponseEntity(responce,HttpStatus.OK);

    }

}
