package org.khmeracademy.webServices.user;

import io.swagger.annotations.Api;
import org.khmeracademy.models.user.Department;
import org.khmeracademy.services.user.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Department API", description = "Department", tags = {"Department API"})
public class DepartmentWebService {

    private DepartmentService departmentService;

    @Autowired
    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/findAllDepartments")
    public ResponseEntity<Map<String, Object>> findAllDepartment() {
        List<Department> departments = new ArrayList<>();
        departments = departmentService.findAllDepartment();
        Map<String, Object> response = new HashMap<>();

        if (departments.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No department data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", "true");
        response.put("message", "Success");
        response.put("data", departments);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
