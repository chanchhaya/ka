package org.khmeracademy.webServices.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.user.HistoryBookmark;
import org.khmeracademy.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "History API", description = "History", tags = {"History API"})
public class HistoryWebService {

    private UserServiceImpl userService;

    @Autowired
    public HistoryWebService(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/history/{userId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", dataType = "int", paramType = "query", required = false, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "limit", dataType = "int", paramType = "query", required = false, defaultValue = "6")
    })
    public ResponseEntity<Map<String, Object>> findAllHistoryByUserID(@PathVariable("userId") Integer userId, @ApiIgnore Pagination pagination) {

        List<HistoryBookmark> historyList = new ArrayList<>();
        Map<String, Object> responce = new HashMap<>();
        historyList = userService.userHistory(userId, pagination);
        if (historyList.isEmpty()) {
            responce.put("status", "false");
            responce.put("message", "No History Data");
            return new ResponseEntity(responce, HttpStatus.NOT_FOUND);
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        responce.put("data", historyList);
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @GetMapping("/countvideohistory/{userId}")
    public ResponseEntity<Map<String, Object>> countVideoBookmark(@PathVariable("userId") Integer userId) {
        Map<String, Object> responce = new HashMap<>();
        Integer countVideohistory = userService.countVideoHistory(userId);
        System.out.println("History: " + countVideohistory);
        if (countVideohistory == 0) {
            responce.put("status", "false");
            responce.put("message", "Video BookMark Not Found");
            return new ResponseEntity(responce, HttpStatus.NOT_FOUND);
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        responce.put("data", countVideohistory);
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @DeleteMapping("/history/delete/{videoId}")
    public ResponseEntity<Map<String,Object>> deleteHistory(@PathVariable("videoId")int videoId){
        Boolean historyDelete;
        Map<String,Object> responce=new HashMap<>();
        historyDelete=userService.historyDelete(videoId);
        if(historyDelete==false){
            responce.put("status","false");
            responce.put("message","History Not Found");
            return new ResponseEntity(responce,HttpStatus.NOT_FOUND );
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        return new ResponseEntity(responce, HttpStatus.OK);
    }

    @PostMapping("/history/insert/{userid}/{videoid}")
    public ResponseEntity<Map<String,Object>> deleteHistory(@PathVariable("userid")Integer userid,@PathVariable("videoid")Integer videoid){
        Boolean historyInsert;
        Map<String,Object> responce=new HashMap<>();
        historyInsert=userService.insertHistory(userid,videoid);
        if(historyInsert==false){
            responce.put("status","false");
            responce.put("message","History Not Found");
            return new ResponseEntity(responce,HttpStatus.NOT_FOUND );
        }
        responce.put("status", "true");
        responce.put("message", "Success");
        return new ResponseEntity(responce, HttpStatus.OK);
    }



}
