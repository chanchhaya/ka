package org.khmeracademy.webServices.user;

import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.Update;
import org.khmeracademy.models.user.User;
import org.khmeracademy.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "User Api", description = "User API", tags = "User API")
public class UserWebService {

    private UserServiceImpl userService;

    @Autowired
    public UserWebService(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PutMapping("/user/update/{user}")
    public ResponseEntity<Map<String, Object>> userUpdate(@PathVariable("user") User user) {
        Boolean userUpdate;
        Map<String, Object> responce = new HashMap<>();
        userUpdate = userService.userUpdate(user);
        if (userUpdate == false) {
            responce.put("status", "false");
            responce.put("message", "Update not success");
            return new ResponseEntity(responce, HttpStatus.NOT_FOUND);
        }
        responce.put("status", "true");
        responce.put("message", "Update Success");
        return new ResponseEntity(responce, HttpStatus.OK);
    }

}
