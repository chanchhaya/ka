package org.khmeracademy.webServices.user;

import io.swagger.annotations.Api;
import org.khmeracademy.models.user.University;
import org.khmeracademy.services.user.UniversiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "University API", description = "University", tags = {"University API"})
public class UniversityWebService {

    private UniversiryService universiryService;

    @Autowired
    public void setUniversiryService(UniversiryService universiryService) {
        this.universiryService = universiryService;
    }

    @GetMapping("/findAllUniversity")
    public ResponseEntity<Map<String, Object>> findAllUniversity() {
        List<University> universities = new ArrayList<>();
        Map<String, Object> response = new HashMap<>();
        universities = universiryService.findAllUniversity();
        if (universities.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No universities data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", "true");
        response.put("message", "Success");
        response.put("data", universities);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
