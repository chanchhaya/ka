package org.khmeracademy.webServices.playlist;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.khmeracademy.models.Pagination;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.playlist.Playlists;
import org.khmeracademy.services.playlist.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/api/v1")
@Api(value = "Playlist API", description = "Playlist", tags = {"Playlist API"})
public class PlaylistWebService {

    private PlaylistService playlistService;

    @Autowired
    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @GetMapping("/search")
    public ResponseEntity<Map<String, Object>> searchByPlaylistName(@RequestParam(value = "keyword", required = false) String name) {
        System.out.println("Url: " + name);
        List<Playlists> playlists = new ArrayList<>();
        playlists = playlistService.searchByPlaylistName(name);
        Map<String, Object> response = new HashMap<>();

        if (playlists.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No playlist data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", "true");
        response.put("message", "Success");
        response.put("data", playlists);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/playlist/{categoryId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", dataType = "int", paramType = "query", required = false, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "limit", dataType = "int", paramType = "query", required = false, defaultValue = "12")
    })
    public ResponseEntity<Map<String, Object>> getPlaylists(@PathVariable("categoryId") int categoryId, @ApiIgnore Pagination pagination) {
        List<Playlists> playlists = playlistService.getPlaylists(categoryId, pagination);
        Map<String, Object> response = new HashMap<>();
        if (playlists.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No playlist to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", playlists);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/countPlaylists/{categoryId}")
    public ResponseEntity<Map<String, Object>> getAllPlaylist(@PathVariable("categoryId") int categoryId) {
        Integer playlists = playlistService.getAllPlaylists(categoryId);
        Map<String, Object> response = new HashMap<>();
        if (playlists == null) {
            response.put("status", "false");
            response.put("message", "No playlist to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", playlists);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/searchByPlaylistName/{playlistName}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "page", dataType = "int", paramType = "query", required = false, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "limit", dataType = "int", paramType = "query", required = false, defaultValue = "16"),
            @ApiImplicitParam(name = "playlistName", value = "playlistName", dataType = "String", paramType = "query", required = false, defaultValue = " ")
    })
    public ResponseEntity<Map<String, Object>> searchResultPlaylist(@PathVariable("playlistName") String playlistName, @ApiIgnore Pagination pagination) {
        List<Playlists> searchResultPlaylist = playlistService.searchResultPlaylist(playlistName, pagination);
        Map<String, Object> response = new HashMap<>();
        if (searchResultPlaylist.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No result");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", searchResultPlaylist);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/countPlaylistByName/{playlistName}")
    public ResponseEntity<Map<String, Object>> countAllPlaylistSearch(@PathVariable("playlistName") String playlistName) {
        Integer playlists = playlistService.countAllPlaylistSearch(playlistName);
        Map<String, Object> response = new HashMap<>();
        if (playlists == null) {
            response.put("status", "false");
            response.put("message", "No result");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", playlists);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
