package org.khmeracademy.webServices.playlist;

import io.swagger.annotations.Api;
import org.khmeracademy.models.playlist.SearchEntity;
import org.khmeracademy.services.playlist.SuggestionSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(description = "search playlist", tags = "SEARCH PlAYLISTS")
public class SearchWebService {

    private SuggestionSearchService searchService;

    @Autowired
    public void setSearchService(SuggestionSearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/suggestion")
    public ResponseEntity<Map<String, Object>> findOneVoteVideoByUser(@RequestParam(value = "name",required = false) String name) {
        Map<String, Object> response = new HashMap<>();
        List<SearchEntity> searchEntities=searchService.findAllSearchFiller(name);
        if (searchEntities == null) {
            response.put("status", false);
            response.put("message", "Empty Data...");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("data", searchEntities);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
