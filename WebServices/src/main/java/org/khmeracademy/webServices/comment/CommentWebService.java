package org.khmeracademy.webServices.comment;
import io.swagger.annotations.Api;
import org.khmeracademy.models.comment.Comment;
import org.khmeracademy.repositoy.comment.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Comment API", description = "Comment", tags = {"Comment API"})
public class CommentWebService {
    private CommentService commentService;

    @Autowired
    public CommentWebService(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/comments/{videoId}")
    public ResponseEntity<Map<String, Object>> getComment(@PathVariable("videoId") int videoId) {
        List<Comment> comments = commentService.getComment(videoId);
        Map<String, Object> response = new HashMap<>();
        if (comments.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", comments);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/comments/reply/{commentId}")
    public ResponseEntity<Map<String, Object>> getReplyComment(@PathVariable("commentId") int commentId) {
        List<Comment> reply = commentService.getReplyComment(commentId);
        Map<String, Object> response = new HashMap<>();
        if (reply.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", reply);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping("/comments/save")
    public ResponseEntity<Map<String, Object>> saveComment(@RequestBody Comment comment) {
        int saveComment = commentService.saveComment(comment);
        Map<String, Object> response = new HashMap<>();
        if (saveComment<1) {
            response.put("status", "false");
            response.put("message", "No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", saveComment);
        return new ResponseEntity(response, HttpStatus.OK);
    }


}
