package org.khmeracademy.webServices;

import org.khmeracademy.models.Video;
import org.khmeracademy.services.ListVideoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ListVideoController {
    private ListVideoService listVideoService;

    public ListVideoController(ListVideoService listVideoService) {
        this.listVideoService = listVideoService;
    }

    @GetMapping("/listvideos/{id}")
    public ResponseEntity<Map<String, Object>> videos(@PathVariable("id") Integer id) {
        Map<String, Object> response = new HashMap<>();
        if (listVideoService.listVideo(id).isEmpty() || listVideoService.listVideo(id) == null) {
            response.put("status", "false");
            response.put("message", "No data to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", listVideoService.listVideo(id));
        return new ResponseEntity(response, HttpStatus.OK);
    }



    //Count Like, Count View, Title of vdo
    @GetMapping("/video/{youtubeid}")
    public ResponseEntity<Map<String, Object>> video(@PathVariable("youtubeid") String youtubeid) {
        Map<String, Object> response = new HashMap<>();
        Video video =listVideoService.video(youtubeid);
        if (video==null) {
            response.put("status", "false");
            response.put("message", "No data to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", video);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
