package org.khmeracademy.webServices;

import io.swagger.annotations.Api;
import org.khmeracademy.models.Vote;
import org.khmeracademy.services.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(description = "vote", tags = "VOTE")
public class VoteWebService {


    private VoteService voteService;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    @GetMapping("/user/video/votetype/{userId}/{videoId}")
    public ResponseEntity<Map<String, Object>> findOneVoteVideoByUser(@PathVariable("userId") Integer userId, @PathVariable("videoId") Integer videoId) {
        Map<String, Object> response = new HashMap<>();
        Vote vote = voteService.findOneVoteVideoByUser(userId, videoId);
        if (vote == null) {
            response.put("status", false);
            response.put("message", "No data to see now...");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", vote);
        return new ResponseEntity(response, HttpStatus.OK);
    }


    @PutMapping("/user/votetype/update/{userId}/{voteType}/{videoId}")
    public Map<String, Object> updateVoteByStatusByUserID(
            @PathVariable("userId") Integer userId,
            @PathVariable("voteType") Integer voteType,
            @PathVariable("videoId") Integer videoId
    ) {
        Map<String, Object> response = new HashMap<>();
        boolean status = voteService.updateVoteByStatusByUserID(userId, voteType, videoId);
        System.out.println("status:"+status);
        if (status == false) {
            response.put("status", false);
            response.put("message", "Update data failed!");
        } else {
            response.put("status", true);
            response.put("message", "Update data successfully!");
        }
        return response;
    }

    @PostMapping("/insert/vote")
    public Map<String, Object> insertVote(@RequestBody Vote vote) {
        Map<String, Object> response = new HashMap<>();
        System.out.println("Class:"+vote);
        if (voteService.insertVote(vote)) {
            response.put("status", true);
            response.put("message", "Insert successfully!");
        } else {
            response.put("status", false);
            response.put("message", "Inserting vote failed!");
        }
        return response;
    }


}
