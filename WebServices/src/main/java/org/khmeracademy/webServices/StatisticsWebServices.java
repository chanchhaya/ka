package org.khmeracademy.webServices;

import org.khmeracademy.services.StatisticServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class StatisticsWebServices {
    @Autowired
    private StatisticServices statistic;

    @GetMapping("/statistics")
    public ResponseEntity<Map<String, Object>> statistics() {
        Map<String, Object> response = new HashMap<>();
        if (statistic.statistics().isEmpty()) {
            response.put("status", "false");
            response.put("message", "No data to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", statistic.statistics());
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
