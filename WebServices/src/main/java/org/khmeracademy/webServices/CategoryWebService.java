package org.khmeracademy.webServices;


import io.swagger.annotations.Api;
import org.khmeracademy.models.category.CategoryForm;
import org.khmeracademy.models.category.Category;
import org.khmeracademy.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Api(description = "category", tags = {"CATEGORY"})
public class CategoryWebService {

    private CategoryServices categoryServices;

    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }


    @GetMapping("/elearning/level1")
    public ResponseEntity<Map<String, Object>> getcategorieslevel1() {
        List<CategoryForm> categories = categoryServices.getcategorieslevel1(0);
        Map<String, Object> response = new HashMap<>();
        if (categories.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", categories);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/elearning/course/{id}")
    public ResponseEntity<Map<String, Object>> getcategorieslevel2_3(@PathVariable("id") int id) {
        List<CategoryForm> categoryLevel2And3s = categoryServices.getcategorieslevel1(id);
        Map<String, Object> response = new HashMap<>();
        if (categoryLevel2And3s.isEmpty()) {
            response.put("status", "false");
            response.put("message", "No category to see now...");
            return new ResponseEntity(response, HttpStatus.NO_CONTENT);
        }
        response.put("status", "true");
        response.put("message", "success");
        response.put("data", categoryLevel2And3s);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/section-two")
    public ResponseEntity<Map<String, Object>> findAllSectionTwo() {
        Map<String, Object> response = new HashMap<>();
        List<Category> sectionTwo = categoryServices.findSectionTwo();
        if (sectionTwo.isEmpty()) {
            response.put("status", false);
            response.put("message", "success");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("data", sectionTwo);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/section-three")
    public ResponseEntity<Map<String, Object>> findAllSectionThree(){
        Map<String, Object> response = new HashMap<>();
        List<Category> sectionThree = categoryServices.findSectionThree();
        if(sectionThree.isEmpty()){
            response.put("status", false);
            response.put("message", "success");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("data", sectionThree);
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
